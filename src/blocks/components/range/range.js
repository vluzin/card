const rangeBtn = document.querySelectorAll('.range__btn');
const rangeBtnPlus = 'range__btn--plus';
const rangeBtnMinus = 'range__btn--minus';
const rangeInput = '.range__input';
const rangeInputs = document.querySelectorAll(rangeInput);

const cardRange = (el) => {
    let this_input = el.parentNode.querySelector(rangeInput);
    
    if (el.classList.contains(rangeBtnPlus)){
        // Прибавляем
        let count = parseInt(this_input.value) + 1;
        
        // Максимальное ограничение
        count = count > parseInt(this_input.max) ? parseInt(this_input.max) : parseInt(this_input.value.length) == 0 ? 1 : count;
        this_input.value = count;
    }
    else{
        // Отнимаем
        let count = parseInt(this_input.value) - 1;
        
        // Минимальное ограничение
        count = count < 1 ? 1 : parseInt(this_input.value.length) == 0 ? 1 : count;
        this_input.value  = count;
    }
}

document.addEventListener('DOMContentLoaded', () => {
    // Клик по кнопкам
    for(let i = 0 ; i < rangeBtn.length ; i++) {
        let btn = rangeBtn[i]; 
        btn.addEventListener('click', (e) => cardRange(e.target))
    }

    // Ограничение ввода кол-ва симолов
    for(let i = 0 ; i < rangeInputs.length ; i++) {
        let input = rangeInputs[i]; 
        input.addEventListener('keyup', (e) => {
            let max = e.target.getAttribute('data-maxlength')
            e.target.value = e.target.value.slice(0, max)
        });   
    }
});